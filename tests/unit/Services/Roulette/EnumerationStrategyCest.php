<?php

namespace Services\Roulette;


use AppBundle\Services\Roulette\EnumerationStrategy;
use Symfony\Component\Process\Exception\InvalidArgumentException;

class EnumerationStrategyCest
{
    public function _before(\UnitTester $I)
    {
    }

    public function _after(\UnitTester $I)
    {
    }

    // tests
    public function tryToCheckArgumentsTest(\UnitTester $I)
    {
        $I->expectException(InvalidArgumentException::class, function () {
            (new EnumerationStrategy(5, 0, function (){}))->run();
        });

        $I->expectException(InvalidArgumentException::class, function () {
            (new EnumerationStrategy(5, 6, function (){}))->run();
        });

        if (PHP_INT_SIZE === 4) {
            $maxPow2 = 32;
        } else {
            $maxPow2 = 64;
        }

        $I->expectException(InvalidArgumentException::class, function () use ($maxPow2) {
            (new EnumerationStrategy(++$maxPow2, 6, function (){}))->run();
        });
    }

    public function tryToCountTest(\UnitTester $I)
    {
        $counter = new EnumerationStrategy(9, 8, function (){});
        $counter->run();
        $I->assertEquals($counter->getResultsCount(), 9);
    }
}
