<?php

namespace AppBundle\Command;

use AppBundle\Services\Roulette\EnumerationStrategy;
use AppBundle\Services\Roulette\RecursiveStrategy;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RouletteCommand
 * @package AppBundle\Command
 */
class RouletteCommand extends Command
{
    const MIN_VARIANTS = 10;

    /**
     *  Количество строк, по достижении которого происходит запись в файл
     */
    const BLOCK_TO_WRITE = 100000;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:roulette')
            ->setDescription('Test task')
            ->addArgument('fieldsCount', InputArgument::REQUIRED, 'Kоличество ячеек.')
            ->addArgument('chipCount', InputArgument::REQUIRED, 'Kоличество фишек.')
            ->addArgument('recursive', InputArgument::OPTIONAL, 'Рекурсивным алгоритмом.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fieldsCount = (int)$input->getArgument('fieldsCount');
        $chipCount = (int)$input->getArgument('chipCount');

        $resultsDir = realpath($this->getApplication()->getKernel()->getRootDir() . '/../var/roulette');
        if (!is_dir($resultsDir)) {
            throw new \Exception('Не существует ' . $resultsDir);
        }
        $fileName = $resultsDir . '/results-' . $fieldsCount . '-' . $chipCount . '.txt';

        $fp = fopen($fileName, 'w');
        if (!flock($fp, LOCK_EX)) {
            throw new \Exception($fileName . ' не удалось блокировать');
        }

        $strAccumulator = '';

        $variantProcess = function ($str) use (&$fp, &$strAccumulator) {
            static $i = 0;
            $strAccumulator .= $str . PHP_EOL;
            if (++$i === self::BLOCK_TO_WRITE) {
                fwrite($fp, $strAccumulator);
                $strAccumulator = '';
                $i = 0;
            }
        };

        $rouletteCounter = (bool)$input->getArgument('recursive')
            ? new RecursiveStrategy($fieldsCount, $chipCount, $variantProcess)
            : new EnumerationStrategy($fieldsCount, $chipCount, $variantProcess);

        $resultsCount = $rouletteCounter->getResultsCount();

        if ($resultsCount < self::MIN_VARIANTS) {
            fwrite($fp, 'Менее ' . self::MIN_VARIANTS . ' вариантов' . PHP_EOL);
            flock($fp, LOCK_UN);
            fclose($fp);
            $output->writeln('Менее ' . self::MIN_VARIANTS . ' вариантов' . PHP_EOL);
            return;
        }

        fwrite($fp, $resultsCount . PHP_EOL);

        $rouletteCounter->run();

        fwrite($fp, $strAccumulator);
        fflush($fp);
        flock($fp, LOCK_UN);
        fclose($fp);

        $output->writeln($resultsCount);
    }

}
