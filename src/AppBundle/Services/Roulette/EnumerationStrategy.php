<?php

namespace AppBundle\Services\Roulette;


use Symfony\Component\Process\Exception\InvalidArgumentException;

/**
 * Class EnumerationStrategy
 * @package AppBundle\Services\Roulette
 */
class EnumerationStrategy extends AbstractStrategy
{
    /**
     * @return void
     */
    public function run()
    {
        $fieldsInt = pow(2, $this->fieldsCount);
        for ($i = 1; $i <= $fieldsInt; ++$i) {
            $v = $i;
            //Counting bits set, Brian Kernighan's way
            for ($c = 0; $v; ++$c) {
                $v &= $v - 1;
            }
            if ($this->chipCount === $c) {
                $result = str_pad(decbin($i), $this->fieldsCount, '0', STR_PAD_LEFT);
                call_user_func($this->variantProcess, $result);
            }
        }
    }

    /**
     *
     */
    protected function checkArguments()
    {
        parent::checkArguments();

        // или пилить длинную арифметику GMP
        if ($this->fieldsCount > ((PHP_INT_SIZE * 8) - 2)) {
            throw new InvalidArgumentException('Значение превышает допустимое целое для системы');
        }
    }

}