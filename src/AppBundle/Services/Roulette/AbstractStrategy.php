<?php

namespace AppBundle\Services\Roulette;


use Symfony\Component\Process\Exception\InvalidArgumentException;

/**
 * Class AbstractStrategy
 * @package AppBundle\Services\Roulette
 */
abstract class AbstractStrategy
{
    /**
     * @var int
     */
    protected $fieldsCount;

    /**
     * @var int
     */
    protected $chipCount;

    /**
     * @var int
     */
    private $resultsCount;

    /**
     * @var callable
     */
    protected $variantProcess;

    /**
     * AbstractStrategy constructor.
     * @param int $fieldsCount
     * @param int $chipCount
     * @param callable $variantProcess
     */
    public function __construct(int $fieldsCount, int $chipCount, callable $variantProcess)
    {
        $this->fieldsCount = $fieldsCount;
        $this->chipCount = $chipCount;
        $this->variantProcess = $variantProcess;
        $this->checkArguments();
    }

    /**
     * @return void
     */
    abstract public function run();


    /**
     * @return int
     */
    public function getResultsCount(): int
    {
        if ($this->resultsCount) {
            return $this->resultsCount;
        }
        $n = $this->chipCount;
        $k = $this->fieldsCount;
        $up = 1;
        for ($i = $k, $c = $k - ($n - 1); $i >= $c; --$i) {
            $up *= $i;
        }
        $fact = 1;
        for ($i = $n; $i >= 1; --$i) {
            $fact *= $i;
        }
        return $this->resultsCount = round($up / $fact);
    }

    /**
     *
     */
    protected function checkArguments()
    {
        if (!($this->fieldsCount && $this->chipCount) || $this->fieldsCount < 0 || $this->chipCount < 0) {
            throw new InvalidArgumentException('Kоличество ячеек и количество фишек должны быть целыми положительными числами');
        }

        if ($this->fieldsCount < $this->chipCount) {
            throw new InvalidArgumentException('Kоличество ячеек должно быть больше количества фишек');
        }
    }

}