<?php

namespace AppBundle\Services\Roulette;


/**
 * Class RecursiveStrategy
 * @package AppBundle\Services\Roulette
 */
class RecursiveStrategy extends AbstractStrategy
{
    /**
     *
     */
    public function run()
    {
        $strBegin = str_repeat('0', $this->fieldsCount - $this->chipCount);
        $strEnd = str_repeat('1', $this->chipCount);
        $this->runRecursive($strBegin . $strEnd);
    }

    /**
     * @param string $str
     * @param string $begin
     */
    private function runRecursive(string $str, string $begin = '')
    {
        call_user_func($this->variantProcess, $begin . $str);
        while ($firstPosition = strpos($str, '1')) {
            $str[$firstPosition - 1] = '1';
            $str[$firstPosition] = '0';
            $newBegin = $begin . substr($str, 0, $firstPosition);
            $newStr = substr($str, $firstPosition);
            $this->runRecursive($newStr, $newBegin);
        }
    }

}